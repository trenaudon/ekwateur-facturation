package org.example.modele;

import org.example.strategie.StrategieTarificationFactory;

public class ClientPerso extends Client {
    private final String civilite;
    private final String prenom;
    private final String nom;

    public ClientPerso(String refClient, String civilite, String prenom, String nom) {
        super(refClient, StrategieTarificationFactory.getStrategiePerso());
        this.civilite = civilite;
        this.prenom = prenom;
        this.nom = nom;
    }
}
