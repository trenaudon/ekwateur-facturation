package org.example.modele;

public class Electricite extends Energie {
    @Override
    public double getTauxClient(Client client) {
        return client.getStrategieTarification().getElectriciteTaux();
    }
}
