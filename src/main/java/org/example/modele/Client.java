package org.example.modele;

import org.example.strategie.StrategieTarification;

public abstract class Client {
    private final String refClient;
    private StrategieTarification strategieTarification;

    public Client(String refClient, StrategieTarification strategieTarification) {
        this.refClient = refClient;
        this.strategieTarification = strategieTarification;
    }

    public StrategieTarification getStrategieTarification() {
        return strategieTarification;
    }
}
