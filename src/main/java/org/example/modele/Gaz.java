package org.example.modele;

public class Gaz extends Energie {
    @Override
    public double getTauxClient(Client client) {
        return client.getStrategieTarification().getGazTaux();
    }
}
