package org.example.modele;

import org.example.strategie.StrategieTarificationFactory;

public class ClientPro extends Client {
    private final String siret;
    private final String raisonSociale;
    private double chiffreAffaire;

    public ClientPro(String refClient, String siret, String raisonSociale, double chiffreAffaire) {
        super(refClient, StrategieTarificationFactory.getStrategiePro(chiffreAffaire));
        this.siret = siret;
        this.raisonSociale = raisonSociale;
        this.chiffreAffaire = chiffreAffaire;
    }
}
