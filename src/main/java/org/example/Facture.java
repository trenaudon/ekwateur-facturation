package org.example;

import org.example.modele.Client;
import org.example.modele.Energie;

public class Facture {
    private Client client;
    private Energie energie;
    private double consommation;
    private double montant;

    public Facture(Client client, Energie energie, double consommation) {
        this.client = client;
        this.energie = energie;
        this.consommation = consommation;
        this.montant = consommation * energie.getTauxClient(client);
    }

    public double getMontant() {
        return this.montant;
    }
}
