package org.example.strategie;

public class TarificationProBasCA implements StrategieTarification {
    @Override
    public double getElectriciteTaux() {
        return 0.118;
    }

    @Override
    public double getGazTaux() {
        return 0.113;
    }
}
