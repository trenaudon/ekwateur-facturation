package org.example.strategie;

public class TarificationProHautCA implements StrategieTarification {
    @Override
    public double getElectriciteTaux() {
        return 0.114;
    }

    @Override
    public double getGazTaux() {
        return 0.111;
    }
}
