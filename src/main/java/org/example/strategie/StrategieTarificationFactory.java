package org.example.strategie;

public class StrategieTarificationFactory {
    public static StrategieTarification getStrategiePro(double chiffreAffaire) {
        if (chiffreAffaire > 1_000_000) {
            return new TarificationProHautCA();
        }
        return new TarificationProBasCA();
    }

    public static StrategieTarification getStrategiePerso() {
        return new TarificationParticulier();
    }
}
