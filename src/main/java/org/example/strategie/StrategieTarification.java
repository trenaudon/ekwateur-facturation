package org.example.strategie;

public interface StrategieTarification {
    double getElectriciteTaux();
    double getGazTaux();
}
