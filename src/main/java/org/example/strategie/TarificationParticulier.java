package org.example.strategie;

public class TarificationParticulier implements StrategieTarification{
    @Override
    public double getElectriciteTaux() {
        return 0.121;
    }

    @Override
    public double getGazTaux() {
        return 0.115;
    }
}
