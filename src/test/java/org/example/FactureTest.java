package org.example;

import org.example.modele.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FactureTest {

    @Test
    void calculMontantFactureGazClientPerso() {
        // Étant donné un client perso
        Client client = new ClientPerso("EKW12345678", "M", "Patrice", "Laffont");

        // Lorsqu'on souhaite calculer le montant de la facture de gaz pour une consommation donnée
        Facture facture = new Facture(client, new Gaz(), 1000);

        // Alors, le montant de la facture correspond à la consommation multipliée par le taux applicable aux clients perso
        assertEquals(115.0, facture.getMontant());
    }

    @Test
    void calculMontantFactureElectriciteClientPerso() {
        // Étant donné un client perso
        Client client = new ClientPerso("EKW12345678", "M", "Patrice", "Laffont");

        // Lorsqu'on souhaite calculer le montant de la facture d'électricité pour une consommation donnée
        Facture facture = new Facture(client, new Electricite(), 1000);

        // Alors, le montant de la facture correspond à la consommation multipliée par le taux applicable aux clients perso
        assertEquals(121.0, facture.getMontant());
    }

    @Test
    void calculMontantFactureGazClientProHautCA() {
        // Étant donné un client pro avec un C.A. de plus de 1 M
        Client client = new ClientPro("EKW12345678", "SIRET", "SolidCode", 1_000_001);

        // Lorsqu'on souhaite calculer le montant de la facture de gaz pour une consommation donnée
        Facture facture = new Facture(client, new Gaz(), 1000);

        // Alors, le montant de la facture correspond à la consommation multipliée par le taux applicable aux clients pro
        assertEquals(111.0, facture.getMontant());
    }

    @Test
    void calculMontantFactureElectriciteClientProHautCA() {
        // Étant donné un client pro avec un C.A. de plus de 1 M
        Client client = new ClientPro("EKW12345678", "SIRET", "SolidCode", 1_000_001);

        // Lorsqu'on souhaite calculer le montant de la facture d'électricité pour une consommation donnée
        Facture facture = new Facture(client, new Electricite(), 1000);

        // Alors, le montant de la facture correspond à la consommation multipliée par le taux applicable aux clients pro
        assertEquals(114.0, facture.getMontant());
    }
    @Test
    void calculMontantFactureGazClientProBasCA() {
        // Étant donné un client pro avec un C.A. de moins de 1 M
        Client client = new ClientPro("EKW12345678", "SIRET", "SolidCode", 1000);

        // Lorsqu'on souhaite calculer le montant de la facture de gaz pour une consommation donnée
        Facture facture = new Facture(client, new Gaz(), 1000);

        // Alors, le montant de la facture correspond à la consommation multipliée par le taux applicable aux clients pro
        assertEquals(113.0, facture.getMontant());
    }

    @Test
    void calculMontantFactureElectriciteClientProBasCA() {
        // Étant donné un client pro avec un C.A. de moins de 1 M
        Client client = new ClientPro("EKW12345678", "SIRET", "SolidCode", 1000);

        // Lorsqu'on souhaite calculer le montant de la facture d'électricité pour une consommation donnée
        Facture facture = new Facture(client, new Electricite(), 1000);

        // Alors, le montant de la facture correspond à la consommation multipliée par le taux applicable aux clients pro
        assertEquals(118.0, facture.getMontant());
    }
}
